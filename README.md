# Projet _Flagone_

Membres du projet : 
* [Yannick](https://gitlab.picatinny.space/xuxu) = Yannick XU
* [Intestinal](https://gitlab.picatinny.space/Intestinal) = Valentin SCANU
* [Ahmedkawa](https://gitlab.picatinny.space/Ahmedkawa) = Ahmed KHAOUANI

## Choix du sujet : Determination du drapeau

## Liens vers les sites en ligne :
* Production (non disponible) : [flagone.picatinny.space](flagone.picatinny.space)
* Beta : [beta.flagone.picatinny.space](beta.flagone.picatinny.space)

## Technologies :
* Python 3.6
* Rust 1.33.0
* Django 2.1.7

## Installation :
1. Assurez vous d'avoir les dépendances installées :
    * `Python` 3.6 ([Téléchargement](https://www.python.org/downloads/release/python-368/))
    * `Rust` 1.33.0 ou plus ([Téléchargement](https://www.rust-lang.org/tools/install))
    * `git`

2. Clonez la [source](https://gitlab.picatinny.space/esgi/3rd-year-annual-project) avec git :  
    ```
    $ git clone https://gitlab.picatinny.space/esgi/3rd-year-annual-project.git
    $ cd 3rd-year-annual-project
    ```

3. Créez le [venv](https://docs.python.org/3.6/library/venv.html) et installez-y les dépendances [Python](https://www.python.org/) :
    ```
    $ python3.6 -m venv .venv
    (Linux/MacOS) $ source .venv/bin/activate
    (Windows) $ .\.venv\Scripts\activate
    $ pip install -r requirement.txt
    ```
    
4. Lancez le serveur [Django](https://www.djangoproject.com/) :
   ```
   $ python flagone/manage.py runserver
   ```
   Vous devriez observer dans votre console un message ressemblant à :
   ```
   Performing system checks...
   
   System check identified no issues (0 silenced).
   
   You have 12 unapplied migration(s). Your project may not work properly until you apply the migrations for app(s): auth, contenttypes, sessions.
   Run 'python manage.py migrate' to apply them.
   March 15, 2019 - 15:35:18
   Django version 2.1.7, using settings 'flagone.settings'
   Starting development server at http://127.0.0.1:8000/
   Quit the server with CTRL-BREAK.
   ```

5. Accèdez à l'interface en local via l'url :  
    [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

## Déploiement du projet via Docker :
1. Connectez-vous sur le serveur via ssh à l'adresse : picatinny.space.

2. Créer l'image [Docker](https://www.docker.com/) via le [Dockerfile](https://docs.docker.com/engine/reference/builder/) du projet à l'aide de la commande :

   Pour la version stable :
   ```
   $ docker build -f Dockerfile_flagone -t flagone:stable --build-arg branch=master .
   ```

   Pour la version beta :
   ```
   $ docker build -f Dockerfile_flagone -t flagone:beta --build-arg branch=develop .
   ```

3. Lancez les conteneurs [Docker](https://www.docker.com/) :

   Pour la version stable :
   ```
   $ docker run -dit --restart always --name flagone --hostname flagone --network picatinny --ip 192.168.222.155 flagone:stable
   ```

   Pour la version beta :
   ```
   $ docker run -dit --restart always --name flagone_beta --hostname flagone_beta --network picatinny --ip 192.168.222.156 flagone:beta
   ```

   Apres avoir lancé le(s) conteneur(s), il reste quelques configurations [Nginx](https://www.nginx.com/) pour que le mise en ligne soit effectué.

4. Configuration de Nginx :
   * Supprimez la ligne dans le fichier __/etc/nginx/nginx.conf__ incluant `site-enabled`.
   * Créez le fichier __/etc/nginx/conf.d/default.conf__ et ajoutez-y la configuration suivante en la modifiant selon vos besoins :
      ```
      server {
          listen 80;  # Écoute sur le port 80
          client_max_body_size 200m;  # Limite le corps des requêtes à 200 Mo
          location /static/ { root /3rd-year-annual-project/flagone; }  # Indique l'emplacement des fichiers statiques
          location / { proxy_pass http://127.0.0.1:8000; }  # Indique l'emplacement du projet
      }
      ```
   * Démarrez le service Nginx avec la commande `service nginx start`.

//use rand::thread_rng;
//use rand::seq::SliceRandom;
//use std::time::Instant;
//use std::process::exit;
//
//mod images;
//mod linear;
//mod models;

/*
 * Sample count : nombre image total
 * inputCountPerSample : nombre pixel par image (x3 si rgb)
 * Alpha : pas apprentissage
 * Epoch : nb iteration
 * XTrain : tout le pixel toutes les images concaténés (dimension 2)
 * YTrain : toutes les valeurs des images concacténés (dimension 1)
 * W : dimension 1
 * biais : ordonné à l'origine
 *
 * Reponse model : tout les W * tout les X, le tout dans une fonction d'activation (qui retourne 1 ou -1, vrai ou faux)
 */

//static IMAGES_DIMENSIONS: (usize, usize) = (128, 128);
//static _SAMPLE_COUNT: usize = 4000;
//static _INPUT_COUNT_PER_SAMPLE: usize = 150 * 150 * 3;
//static ALPHA: f64 = 0.02;
//static EPOCHS: usize = 1;
//static _BIAIS: usize = 1;


fn main() {
    /*
    // Load image for model training
    println!("Loading french flag images...");
    let (mut x_train, mut y_train) = images::load_from_images_path(
        "../../datasets/france",
        IMAGES_DIMENSIONS,
        1.0
    );
    println!("Loading german flag images...");
    {
        let (x_train_notok, y_train_notok) = images::load_from_images_path(
            "../../datasets/germany",
            IMAGES_DIMENSIONS,
            0.0
        );
        println!("Merging buffers...");
        x_train.extend(x_train_notok);
        y_train.extend(y_train_notok);
    }

    println!("Shuffling images...");
    let mut index_list: Vec<usize> = (0..y_train.len()).collect::<Vec<usize>>();
    index_list.shuffle(&mut thread_rng());

    println!("Initialising W...");
    let mut w: Vec<f64> = linear::create_linear_model(y_train.len());

    let ma_function();

    // Train model
    let now = Instant::now();
    linear::fit_classification_rosenblatt_rule(
        &mut w,
        &x_train,
        &y_train,
        ALPHA,
        EPOCHS,
        &index_list
    );
    println!("                   ");
    println!("{}s", now.elapsed().as_secs());
    exit(0);

    // Test model reliability
    let (france_tests, _) = images::load_from_images_path(
        "../../datasets/france_tests",
        IMAGES_DIMENSIONS,
        1.0
    );
    let (germany_tests, _) = images::load_from_images_path(
        "../../datasets/germany_tests",
        IMAGES_DIMENSIONS,
        0.0
    );

    let mut france_good_result: usize = 0;
    for french_flag in &france_tests {
        if linear::predict_classification(&french_flag, &w) == 1.0 {
            france_good_result += 1;
        }
    }

    let mut germany_good_result: usize = 0;
    for german_flag in &germany_tests {
        if linear::predict_classification(&german_flag, &w) == 0.0 {
            germany_good_result += 1;
        }
    }

    println!("French flag ok : {}/{}", france_good_result, france_tests.len());
    println!("German flag ok : {}/{}", germany_good_result, germany_tests.len());

    let france_test1 = images::load_image("france1.jpg",IMAGES_DIMENSIONS);
    let france_test2 = images::load_image("france2.jpg",IMAGES_DIMENSIONS);
    let not_france_test1 = images::load_image("german1.jpg",IMAGES_DIMENSIONS);
    let not_france_test2 = images::load_image("german2.jpg",IMAGES_DIMENSIONS);

    println!("France 1, expected 1 found {}", linear::predict_classification(&france_test1, &w));
    println!("France 2, expected 1 found {}", linear::predict_classification(&france_test2, &w));
    println!("Not france 1, expected 0 found {}", linear::predict_classification(&not_france_test1, &w));
    println!("Not france 2, expected 0 found {}", linear::predict_classification(&not_france_test2, &w));
    // Save model
    models::save("france.model", &w);*/
}

import numpy as np
import _rustymachine
from .core import Base, CLASSIFICATION, REGRESSION
import json


class Linear(Base):
    def init_w(self):
        if isinstance(self.x_train, np.ndarray):
            if self.x_train.any() and self.x_train[0].any():
                self.model = _rustymachine.linear_utils_create_model(len(self.x_train[0]))
            else:
                self.model = _rustymachine.linear_utils_create_model(len(self.y_train))
        else:
            if self.x_train and self.x_train[0]:
                self.model = _rustymachine.linear_utils_create_model(len(self.x_train[0]))
            else:
                self.model = _rustymachine.linear_utils_create_model(len(self.y_train))

    def fit(self, epochs=1, loss_stop=False, type_=None, alpha=None):
        if alpha:
            self.alpha = alpha

        x_train, y_train = super().fit(type_)

        if self._type == CLASSIFICATION:
            if not self.model:
                self.init_w()

            self.model = _rustymachine.linear_classification_fit(
                self.model,
                x_train,
                y_train,
                self.alpha,
                epochs,
                loss_stop,
            )
            return self
        elif self._type == REGRESSION:
            self.model = _rustymachine.linear_regression_fit(
                x_train,
                y_train,
            )
            return self

    def predict(self, to_predict):
        to_predict = super().predict(to_predict)

        if self._type == CLASSIFICATION:
            return _rustymachine.linear_classification_predict(to_predict, self.model)
        elif self._type == REGRESSION:
            return _rustymachine.linear_regression_predict(to_predict, self.model)

    def save(self, path):
        if not self.model:
            Exception("Your current instance contains no model.")
        else:
            model_saved = json.dumps({
                "model": self.model,
                "alpha": self.alpha,
                "type_": self._type,
            })
            with open(path, "w", encoding="utf-8") as f:
                f.write(model_saved)

    @classmethod
    def load(cls, path):
        with open(path, "r", encoding="utf-8") as f:
            model_getted = json.loads(f.read())

        new_model = cls(
            alpha=model_getted["alpha"],
            type_=model_getted["type_"],
        )
        new_model.model = model_getted["model"]
        return new_model
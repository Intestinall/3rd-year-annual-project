import _rustymachine
import numpy as np
from .core import Base, CLASSIFICATION, REGRESSION
import json


class Mlp(Base):
    def __init__(self, x_train=None, y_train=None, npl=None, alpha=0.01, type_=CLASSIFICATION):
        super().__init__(x_train=x_train, y_train=y_train, alpha=alpha, type_=type_)
        self._npl = npl

        if not npl:
            raise Exception("nlp can't be empty.")

    def init_w(self):
        self.model = _rustymachine.mlp_utils_create_model(self.npl)

    def save(self, path):
        if not self.model:
            raise Exception("Your current instance contains no model.")
        else:
            model_saved = json.dumps({
                "model": self.model,
                "npl": self.npl,
                "alpha": self.alpha,
                "type_": self._type,
            })
            with open(path, "w", encoding="utf-8") as f:
                f.write(model_saved)

    @classmethod
    def load(cls, path):
        with open(path, "r", encoding="utf-8") as f:
            model_getted = json.loads(f.read())

        new_model = cls(
            npl=model_getted["npl"],
            alpha=model_getted["alpha"],
            type_=model_getted["type_"],
        )
        new_model.model = model_getted["model"]
        return new_model

    @property
    def npl(self):
        if (isinstance(self.y_train, np.ndarray) and self.y_train.any()) or self.y_train:
            return [len(self.x_train[0])] + self._npl
        else:
            return self._npl

    def fit(self, epochs=1, loss_stop=False, type_=None, alpha=None):
        if alpha:
            self.alpha = alpha

        x_train, y_train = super().fit(type_)

        if not self.model:
            self.init_w()

        if self._type == CLASSIFICATION:
            if not isinstance(y_train[0], list):
                y_train = [[x] for x in y_train]

            self.model = _rustymachine.mlp_classification_fit(
                self.model,
                x_train,
                y_train,
                self.alpha,
                epochs,
                loss_stop,
                self.npl,
            )
            return self
        elif self._type == REGRESSION:
            if not isinstance(y_train[0], list):
                y_train = [[x] for x in y_train]

            self.model = _rustymachine.mlp_regression_fit(
                self.model,
                x_train,
                y_train,
                self.alpha,
                epochs,
                loss_stop,
                self.npl,
            )
            return self

    def predict(self, to_predict):
        to_predict = super().predict(to_predict)

        if self._type == CLASSIFICATION:
            r = _rustymachine.mlp_classification_predict(to_predict, self.model, self.npl)

            if len(r) > 1:
                max_index = r.index(max(r))
                r = [-1 if i != max_index else 1 for i, x in enumerate(r)]  # TODO(intestinal) Implement in rust

            return r
        elif self._type == REGRESSION:
            return _rustymachine.mlp_regression_predict(to_predict, self.model, self.npl)

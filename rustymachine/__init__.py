from .core import CLASSIFICATION, REGRESSION
from .linear import Linear
from .mlp import Mlp
from .rbf import Rbf
from .kmeans import Kmeans

extern crate cpython;
extern crate nalgebra;

use cpython::{PyResult, Python};
use nalgebra::DMatrix;


fn distance(p1: &[f64], p2: &[f64]) -> f64 {
    let mut r: f64 = 0.0;

    for (p1_n, p2_n) in p1.iter().zip(p2) {
        r += (p1_n - p2_n).powi(2);
    }
    return r.sqrt();
}


fn calcul_phi(x_train: Vec<Vec<f64>>, gamma: f64) -> Vec<Vec<f64>> {
    let sample_count: usize = x_train.len();
    let mut phi: Vec<Vec<f64>> = vec![vec![0_f64; sample_count + 1]; sample_count];

    for i in 0..sample_count {
        for j in 0..sample_count {
            phi[i][j] = (
                (-gamma)
                * distance(&x_train[i], &x_train[j]).powi(2)
            ).exp();
        }
    }
    return phi;
}


pub fn predict(w: Vec<f64>, x_train: Vec<Vec<f64>>, gamma: f64, x_to_predict: Vec<f64>) -> f64 {
    let mut r: f64 = 0_f64;

    for i in 0..x_train.len() {
        r += w[i] * (
            (-gamma)
            * distance(&x_train[i], &x_to_predict).powi(2)
        ).exp();
    }
    return r;
}


pub fn fit(_: Python, x_train: Vec<Vec<f64>>, y_train: Vec<Vec<f64>>, gamma: f64) -> PyResult<Vec<f64>> {
    let phi: Vec<Vec<f64>> = calcul_phi(x_train, gamma);
    let phi_rows: usize = phi.len();
    let phi_cols: usize = phi[0].len();

    let phi_flatten: Vec<f64> = phi.into_iter()
        .flat_map(|x| x.into_iter())
        .collect::<Vec<f64>>();
    let phi_inv = DMatrix::from_vec(
        phi_cols,
        phi_rows,
        phi_flatten
    ).transpose().pseudo_inverse(1e-40_f64).unwrap();

    let y_train_rows: usize = y_train.len();
    let y_train_cols: usize = y_train[0].len();
    let y_train_flatten: Vec<f64> = y_train.into_iter()
        .flat_map(|x| x.into_iter())
        .collect::<Vec<f64>>();
    let y = DMatrix::from_vec(
        y_train_cols,
        y_train_rows,
        y_train_flatten
    ).transpose();

    let w = (phi_inv * y).into_iter().cloned().collect::<Vec<f64>>();
    Ok(w)
}

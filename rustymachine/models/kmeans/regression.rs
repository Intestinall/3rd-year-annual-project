use cpython::{PyResult, Python};

use crate::kmeans;


pub fn predict(_: Python, w: Vec<f64>, x_train: Vec<Vec<f64>>, gamma: f64, x_to_predict: Vec<f64>) -> PyResult<f64> {
    Ok(
        kmeans::common::predict(
            w,
            x_train,
            gamma,
            x_to_predict
        )
    )
}

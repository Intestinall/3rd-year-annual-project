extern crate cpython;
extern crate nalgebra;

use cpython::{PyResult, Python};
use nalgebra::DMatrix;
use rand::Rng;

fn distance(p1: &[f64], p2: &[f64]) -> f64 {
    let mut r: f64 = 0.0;

    for (p1_n, p2_n) in p1.iter().zip(p2) {
        r += (p1_n - p2_n).powi(2);
    }
    return r.sqrt();
}

fn is_cluster(cluster_index: usize, rand_pos_list: &[usize], r: usize) -> bool {

    for i in 0..cluster_index {
        if rand_pos_list[i] == r { return true; }
    }

    return false;
}

pub fn predict(w: Vec<f64>, x_train: Vec<Vec<f64>>, gamma: f64, x_to_predict: Vec<f64>) -> f64 {

    let mut r: f64 = 0_f64;

    for i in 0..x_train.len() {
        r += w[i] * (
            (-gamma)
            * distance(&x_train[i], &x_to_predict).powi(2)
        ).exp();
    }

    return r;
}

fn init_mu(cluster_count: usize, x_train: &[Vec<f64>]) -> Vec<Vec<f64>> {

    let mut mu: Vec<Vec<f64>> = vec![vec![0_f64; x_train[0].len()]; cluster_count];
    let mut rand_pos_list: Vec<usize> = vec![0_usize; cluster_count];

    for i in 0..cluster_count {

        let mut r: usize = 0_usize;

        loop {
            r = rand::thread_rng().gen_range(0, x_train.len());

            if is_cluster(i, &rand_pos_list, r) {
                break;
            }
        }

        //let r: usize = rand::thread_rng().gen_range(0, x_train.len());
        rand_pos_list[i] = r;
        &mu[i].copy_from_slice(&x_train[r]);
    }
    return mu;
}

fn get_set(x_train: &[Vec<f64>], mu: Vec<Vec<f64>>, cluster_count: usize) -> Vec<usize> {
    let mut sets = vec![0_usize; x_train.len()];

    for i in 1..cluster_count {
        for j in 0..x_train.len() {
            let mut is_shorter: bool = true;

            if distance(&x_train[j], &mu[i]) > distance(&x_train[j], &mu[sets[j]]) {
                is_shorter = false;
            }

            if is_shorter {
                sets[j] = i;
            }
        }
    }
    return sets;
}


fn update_mu(cluster_count: usize, sets: Vec<usize>, x_train: &[Vec<f64>]) -> Vec<Vec<f64>> {
    let mut new_mu : Vec<Vec<f64>> = vec![vec![0_f64; x_train[0].len()]; cluster_count];

    for i in 0..cluster_count {
        for j in 0..x_train[0].len() {
            let mut elem_in_cluster_count: f64 = 0_f64;
            let mut val: f64 = 0_f64;

            for k in 0..x_train.len() {
                if sets[k] == i {
                    elem_in_cluster_count += 1.0;
                    val += x_train[k][j];
                }
            }

            val /= elem_in_cluster_count;
            new_mu[i][j] = val;
        }
    }
    return new_mu;
}



fn calcul_phi(x_train: &[Vec<f64>], cluster_count: usize, gamma: f64) -> Vec<Vec<f64>> {
    let sample_count: usize = x_train.len();
    let mut phi: Vec<Vec<f64>> = vec![vec![0_f64; cluster_count + 1]; sample_count];

    for i in 0..sample_count {
        for j in 0..cluster_count {
            phi[i][j] = ((-gamma) * distance(&x_train[i], &x_train[j]).powi(2)).exp();
        }
    }
    return phi;
}

pub fn fit(_: Python, x_train: Vec<Vec<f64>>, y_train: Vec<Vec<f64>>, cluster_count: usize, gamma: f64) -> PyResult<Vec<f64>> {

    let mut sets: Vec<usize>;

    let mut mu: Vec<Vec<f64>> = init_mu(cluster_count, &x_train);

    for _ in 0..100 {
        sets = get_set(&x_train, mu, cluster_count);
        mu = update_mu(cluster_count, sets, &x_train);
    }

    let phi = calcul_phi(&x_train, cluster_count, gamma);

    let phi_rows: usize = phi.len();
    let phi_cols: usize = phi[0].len();

    let phi_flatten: Vec<f64> = phi.into_iter()
        .flat_map(|x| x.into_iter())
        .collect::<Vec<f64>>();

    let y_train_rows: usize = y_train.len();
    let y_train_cols: usize = y_train[0].len();

    let y_train_flatten = y_train.into_iter()
        .flat_map(|x| x.into_iter())
        .collect::<Vec<f64>>();


    let phi_matrix_t = DMatrix::from_vec(phi_rows, phi_cols, phi_flatten);
    let phi_matrix = phi_matrix_t.transpose();

    let y_train_matrix = DMatrix::from_vec(y_train_rows, y_train_cols, y_train_flatten);
    let phi_matrix_inv = (&phi_matrix_t * phi_matrix).pseudo_inverse(1_f64).unwrap();

    let w = (phi_matrix_inv * phi_matrix_t * y_train_matrix).into_iter().cloned().collect::<Vec<f64>>();;

    Ok(w)
}
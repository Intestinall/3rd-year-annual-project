use cpython::PyResult;

use crate::linear;
use crate::mlp;
use crate::misc;


pub fn fit(
    mut w: Vec<Vec<Vec<f64>>>,
    mut x_train: Vec<Vec<f64>>,
    y_train: Vec<Vec<f64>>,
    alpha: f64,
    epochs: usize,
    _loss_stop: bool,
    npl: Vec<usize>,
    feed_forward: &dyn Fn(&mut [Vec<Vec<f64>>], &[f64], &[usize]) -> Vec<Vec<f64>>,
    init_last_deltas: &dyn Fn(Vec<Vec<f64>>, &[Vec<f64>], &[f64], &[usize]) -> Vec<Vec<f64>>,
) -> PyResult<Vec<Vec<Vec<f64>>>> {
    linear::utils::add_biais_x_train(&mut x_train);
    let x_train = x_train;

    let mut deltas: Vec<Vec<f64>> = mlp::common::init_deltas(&npl);

    for epoch in 0..epochs {
        let shuffled_index_list: Vec<usize> = misc::models::generate_shuffled_index_list(y_train.len());
        let mut loss: f64 = 0_f64;

        for (im, k) in shuffled_index_list.iter().map(|x| *x).enumerate() {
            print!("Epoch {:4}/{:4}  => {:4}/{:4}\r", epoch + 1, epochs, im, shuffled_index_list.len());

            let x: Vec<Vec<f64>> = feed_forward(&mut w, &x_train[k], &npl);

            {
                let prediction: &[f64] = &x[x.len() - 1];
                let expected_result: &[f64] = &y_train[k];

                let mut temp_loss: f64 = 0_f64;
                for i in 0..expected_result.len() {
                    temp_loss += (expected_result[i] - prediction[i]).powi(2);
                }
                loss += temp_loss / expected_result.len() as f64;
            }

            deltas = init_last_deltas(deltas, &x, &y_train[k], &npl);
            deltas = mlp::common::init_all_deltas(deltas, &x, &w, &npl);
            mlp::common::update_w(&mut w, &deltas, alpha, &x, &npl);
        }
        print!("Epoch {:4}/{:4} => Loss {:.4}\r\n", epoch + 1, epochs, loss / shuffled_index_list.len() as f64);
    }
    Ok(w)
}


pub fn predict(
    mut x_train_row: Vec<f64>,
    mut w: Vec<Vec<Vec<f64>>>,
    npl: Vec<usize>,
    feed_forward: &dyn Fn(&mut [Vec<Vec<f64>>], &[f64], &[usize]) -> Vec<Vec<f64>>
) -> PyResult<Vec<f64>> {
    x_train_row.insert(0, 1_f64);
    let mut predictions: Vec<f64> = feed_forward(&mut w, &x_train_row, &npl).pop().unwrap();
    predictions.remove(0);
    Ok(predictions)
}

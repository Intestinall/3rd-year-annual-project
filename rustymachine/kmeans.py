import _rustymachine
import numpy as np
from .core import Base, CLASSIFICATION, REGRESSION


class Kmeans(Base):
    def __init__(self, cluster_count, x_train=None, y_train=None, gamma=0.15, type_=CLASSIFICATION):
        super().__init__(x_train=x_train, y_train=y_train, alpha=gamma, type_=type_)
        self._cluster_count = cluster_count

        if not cluster_count:
            raise Exception("cluster_count can't be empty.")

    def fit(self, type_=None):
        x_train, y_train = super().fit(type_)

        if not isinstance(y_train[0], list):
            y_train = [[x] for x in y_train]

        self.model = _rustymachine.kmeans_common_fit(
            x_train,
            y_train,
            self._cluster_count,
            self.alpha,
        )
        return self

    def predict(self, to_predict):
        to_predict = super().predict(to_predict)

        if self._type == CLASSIFICATION:
            return _rustymachine.kmeans_classification_predict(
                self.model,
                self.x_train,
                self.alpha,
                to_predict
            )

        elif self._type == REGRESSION:
            return _rustymachine.kmeans_regression_predict(
                self.model,
                self.x_train,
                self.alpha,
                to_predict
            )

rust_build:
	cd rustymachine/models && \
	RUSTFLAGS="-Ctarget-cpu=native" cargo build --release && \
	cp target/release/lib_rustymachine.so ../../_rustymachine.so && \
	cd ../..

rust_remove_build:
	rm -rf rustymachine/models/target

rust_build_from_scratch: rust_remove_build rust_build

rust_test_compilation:
	cd rustymachine/models && \
	cargo build && \
	cd ../..

run_django:
	python flagone/manage.py runserver

run_jupyter:
	jupyter notebook

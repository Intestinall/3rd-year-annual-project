FROM python:3.7.2

ARG branch

RUN apt-get -y update && apt-get install -y curl nano git nginx

RUN git clone -b ${branch} https://gitlab.picatinny.space/esgi/3rd-year-annual-project.git

WORKDIR 3rd-year-annual-project/flagone
RUN pip install -r ../requirement.txt

CMD ["gunicorn", "flagone.wsgi:application", "--bind=127.0.0.1:8000", "--reload"]

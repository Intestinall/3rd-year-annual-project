$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (settings.type === "POST" && !this.crossDomain) {
            xhr.setRequestHeader('X-CSRFToken', $('[name=csrfmiddlewaretoken]').val());
        }
    }
});

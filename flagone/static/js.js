$("#hihihi").on("click", function () {
    const formData = new FormData();
    const file = $('#upload')[0].files[0];
    if (!file) { return; }

    formData.append('image', file, file.name);

    $.ajax({
        type:'POST',
        url: "/hihihi",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(data) {
            alert(data["r"]);
        },
        error: function(data) {
            if (data.responseJSON["error"]) {
                alert(data.responseJSON["error"]);
            } else {
                alert("An error occurred during the process.");
            }
        }
    });
});

$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading"); },
    ajaxStop: function() { $body.removeClass("loading"); }
});

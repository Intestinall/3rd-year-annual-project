from PIL import Image
from io import BytesIO

IMAGE_SIZE = 128


def get_pixels(bytesio):
    pixels = []
    image = Image.open(bytesio).resize((IMAGE_SIZE, IMAGE_SIZE), Image.ANTIALIAS).convert('RGB')

    width, height = image.size

    for x in range(width):
        for y in range(height):
            pixels.extend((
                v / 255
                for v in image.getpixel((x, y))
            ))
    return pixels

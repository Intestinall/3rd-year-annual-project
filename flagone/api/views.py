import os
import sys

from django.http import JsonResponse
from django.shortcuts import render

from .utils import get_pixels

sys.path.append(os.path.abspath(os.path.join(__file__, "../../..")))
import rustymachine


ALLOWED_MIMES = {
    "image/jpeg",
    "image/gif",
    "image/png",
}


def basement(request):
    return render(request, "basement.html")


def hihihi(request):
    image = request.FILES.get("image")
    allowed_mime = image.content_type in ALLOWED_MIMES

    if image and allowed_mime:
        """
        YOU NEED TO ADD FUNCTION TO PROCESS THE IMAGE HERE
        """
        inputs = get_pixels(image.file)

        model = rustymachine.Linear.load(os.path.join("trained_models", "linear_classification_128_128_france_germany_france.model"))
        prediction = model.predict(inputs)

        if prediction > 0.6:
            r = "France"
        elif prediction < -0.6:
            r = "Allemagne"
        else:
            r = "Inconnu"

        return JsonResponse({"r": r}, status=201)
    else:
        if not allowed_mime:
            return JsonResponse({"error": f'"{image.content_type}" files are not supported.'}, status=400)
        elif not image:
            return JsonResponse({"error": "You need to upload an image."}, status=400)
        else:
            return JsonResponse({"other": "An error occurred during the process."}, status=400)